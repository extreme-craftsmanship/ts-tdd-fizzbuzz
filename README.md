# TDD @FizzBuzz

## 开发环境
 - node v14
 - TypeScript
 - Jest
 
## 业务目标

### FizzBuzz
设计一个程序，该程序能打印1到100之间的数字，包含1，100。
- Given 需要打印的数字是6（3的倍数），When 打印，Then 打印`"Fizz"`
- Given 需要打印的数字是10（5的倍数），When 打印，Then 打印`"Buzz"`
- Given 需要打印的数字是15（同时是3和5的倍数），When 打印，Then 打印`"FizzBuzz"`
- Given 需要打印的数字是2（不是3或者5的倍数），When 打印，Then 打印`"2"`
- Given 需要打印的数字是100（超出限制），When 打印，Then 抛出异常

## Tasking

| Task | Input | Output |
|:---|:---|:---|
| 1 | 15, 30, 45, 60, 75, 90 |  `"FizzBuzz"` |
| 2 | 3, 6, 12, 18, 21, 99 |  `"Fizz"` |
| 3 | 5, 10, 20, 85, 95, 100 |  `"Buzz"` |
| 4 | 1, 2, 4, 94, 97, 98 |  `"1"`, `"2"`, `"4"`, `"94"`, `"97"`, `"98"` |
| 5 | 0, 101 |  `RangeError` |

## 编码路线

## 参考资料
- [Jest 用户指南](https://jestjs.io/zh-Hans/docs/getting-started)